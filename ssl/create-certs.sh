#!/bin/bash

set -o nounset \
    -o errexit \
    -o verbose \
    -o xtrace

SCRIPT_DIR=$(pwd)
SECRETS_DIR="$SCRIPT_DIR/secrets"

# Remove any old content
cd $SECRETS_DIR

if [ "$(ls -A $SECRETS_DIR)" ]; then
	rm *
fi

# Generate CA key
openssl req -new -x509 -keyout snakeoil-ca-1.key -out snakeoil-ca-1.crt -days 365 -subj '/CN=ca1.test.scalytica.net/OU=TEST/O=SCALYTICA/L=Asker/S=null/C=NO' -passin pass:scalytica -passout pass:scalytica

# Kafkacat
openssl genrsa -des3 -passout "pass:scalytica" -out kafkacat.client.key 1024
openssl req -passin "pass:scalytica" -passout "pass:scalytica" -key kafkacat.client.key -new -out kafkacat.client.req -subj '/CN=kafkacat.test.scalytica.net/OU=TEST/O=SCALYTICA/L=Asker/S=null/C=NO'
openssl x509 -req -CA snakeoil-ca-1.crt -CAkey snakeoil-ca-1.key -in kafkacat.client.req -out kafkacat-ca1-signed.pem -days 9999 -CAcreateserial -passin "pass:scalytica"

for i in broker1 producer consumer
do
	echo $i
	# Create keystores
	echo "Creating keystores..."
	keytool -genkey -noprompt \
				 -alias $i \
				 -dname "CN=$i.test.scalytica.net, OU=TEST, O=SCALYTICA, L=Asker, S=null, C=NO" \
				 -keystore kafka.$i.keystore.jks \
				 -keyalg RSA \
				 -storepass scalytica \
				 -keypass scalytica

	# Create CSR, sign the key and import back into keystore
	keytool -keystore kafka.$i.keystore.jks -alias $i -certreq -file $i.csr -storepass scalytica -keypass scalytica
	openssl x509 -req -CA snakeoil-ca-1.crt -CAkey snakeoil-ca-1.key -in $i.csr -out $i-ca1-signed.crt -days 9999 -CAcreateserial -passin pass:scalytica
	echo yes | keytool -keystore kafka.$i.keystore.jks -alias CARoot -import -file snakeoil-ca-1.crt -storepass scalytica -keypass scalytica
	echo yes | keytool -keystore kafka.$i.keystore.jks -alias $i -import -file $i-ca1-signed.crt -storepass scalytica -keypass scalytica

	# Create truststore and import the CA cert.
	echo yes | keytool -keystore kafka.$i.truststore.jks -alias CARoot -import -file snakeoil-ca-1.crt -storepass scalytica -keypass scalytica

  echo "scalytica" > ${i}_sslkey_creds
  echo "scalytica" > ${i}_keystore_creds
  echo "scalytica" > ${i}_truststore_creds
done

cd $SCRIPT_DIR
