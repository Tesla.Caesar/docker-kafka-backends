# Kafka and Zookeeper in Docker

This project provides different scripts and configurations for running kafka and zookeeper in docker. The main purpose
is to aid developers get started with kafka. It mainly uses docker-compose to spin up the different nodes.

**DO NOT BASE YOUR PRODUCTION SYSTEMS ON ANY OF THESE CONFIGURATIONS**

