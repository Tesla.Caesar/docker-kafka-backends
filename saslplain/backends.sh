#!/usr/bin/env bash

CMD=$1

CURR_DIR=$(pwd)
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Prepare SASL / JAAS configuration
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SASL_DIR="$SCRIPT_DIR/sasl"
JAAS_CONFIG="kafka_server_jaas.conf"
SASL_JAAS_CONFIG="$SASL_DIR/$JAAS_CONFIG"

mkdir -p $SASL_DIR

if [ ! -f $SASL_JAAS_CONFIG ]; then
  echo 'KafkaServer {
    org.apache.kafka.common.security.plain.PlainLoginModule required
    username="admin"
    password="admin-pass"
    user_admin="admin-pass"
    user_luke="luke-pass"
    user_vader="vader-pass";
  };
  Client {}' > $SASL_JAAS_CONFIG
else
  echo "Using existing $JAAS_CONFIG file"
fi

export KAFKA_SASL_DIR=$SASL_DIR

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Start containers defined in docker-compose file
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function start() {
  echo "Starting docker-compose build..."
  docker-compose up -d --build --force-recreate
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Take down containers and remove any containers build by docker-compose
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function stop() {
  echo "Stopping services..."
  docker-compose down --remove-orphans --rmi local
}

cd ${SCRIPT_DIR}

if [ "$CMD" = "start" ]; then
  start

elif [ "$CMD" = "stop" ]; then
  stop

elif [ "$CMD" = "restart" ]; then
  stop
  start

else
  echo "Usage: $ backends.sh <start | stop | restart>"
fi

cd ${CURR_DIR}