#!/usr/bin/env bash

CMD=$1
FLAG=$2

CURR_DIR=$(pwd)
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
ROOT_DIR=$( cd $SCRIPT_DIR/../..; pwd )

if [[ "$(uname)" == "Darwin" ]]; then
  export HOST_IP=$(ipconfig getifaddr en0)
elif [[ "$(uname)" == "Linux" ]]; then
  export HOST_IP=$(ip route get 1 | awk '{print $NF;exit}')
else
  echo "Windows not supported..."
  exit 1
fi

# Start containers defined in docker-compose file
function start() {
  echo "Starting docker-compose build..."
  docker-compose up -d --build --force-recreate
}

# Take down containers and remove any containers build by docker-compose
function stop() {
  echo "Stopping services..."
  docker-compose down --remove-orphans --rmi local
}

cd $SCRIPT_DIR

if [ "$CMD" = "start" ]; then
  start

elif [ "$CMD" = "stop" ]; then
  stop

elif [ "$CMD" = "restart" ]; then
  stop
  start

else
  echo "Usage: $ backends.sh <start | stop | restart>"
fi

cd $CURR_DIR